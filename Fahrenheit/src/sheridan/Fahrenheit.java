package sheridan;

public class Fahrenheit {
	public static int fromCelsius(int temp) {
		return Math.round(temp * (9f / 5) + 32);
	}
}
