package sheridan;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
//Deep Navadiya - 991550374
class FahrenheitTest {

	@Test
	void fromCelsiusRegular() {
		int result = Fahrenheit.fromCelsius(0);
		assertTrue("Result Failed", result == 32);
	}
	@Test
	void fromCelsiusExceptional() {
		int result = Fahrenheit.fromCelsius(0);
		assertFalse("Result Failed", result >= 33);
	}
	@Test
	void fromCelsiusBoundaryIn() {
		int result = Fahrenheit.fromCelsius(-18);
		assertTrue("Result Failed", result == -0);
	}
	@Test
	void fromCelsiusBoundaryOut() {
		int result = Fahrenheit.fromCelsius(1);
		assertFalse("Result Failed", result == 33);
	}

}
